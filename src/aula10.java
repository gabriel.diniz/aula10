import java.util.Scanner;

public class aula10 {

    public static void main(String[] args) {

        int num;  //variavel

        /*
        num = 10;

        if (num == 10){ //comparação , if se o numero for igual a 10, imprime a mensagem sim
            System.out.println("Sim, o número é igual.");
        }else { //se não
            System.out.println("Não, o número não é igual."); // imprime a mensagem não
        */

        System.out.println("Digite o número 1!"); // imprime a mensagem

        Scanner in = new Scanner(System.in);

        num = in.nextInt(); // num recebe in.nextInt

        if (num==1) { //se num for igual a 1
            System.out.println("Obrigado por digitar o número 1!"); // imprime a mensagem sim
        }else{ // se não
            System.out.println("O número digitado não é igual a 1!"); // imprime a mensagem não

        }
    }
}
